<?php

namespace Ring\Support\Enum;

class Gender {

  const MALE    = 0;
  const FEMALE     = 1;
}
