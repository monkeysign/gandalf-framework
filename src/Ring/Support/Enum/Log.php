<?php

namespace Ring\Support\Enum;

class Log {
    const EMERGENCY = "EMERGENCY";
    const ALERT     = "ALERT";
    const CRITICAL  = "CRITICAL";
    const ERROR     = "ERROR";
    const WARN      = "WARN";
    const NOTICE    = "NOTICE";
    const INFO      = "INFO";
    const DEBUG     = "DEBUG";

}



    