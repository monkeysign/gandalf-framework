<?php

namespace Ring\Support\Enum;

class Role {

    const SUPERADMIN = 0;
    const ADMIN = 1;
	const EDITOR = 2;
	const CUSTOMER = 7;
	const MEMBER = 8;
    const USER = 9;

}
