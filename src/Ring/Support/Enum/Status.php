<?php

namespace Ring\Support\Enum;

class Status {
    /**
     * var disabilittato
     */
  const DISABLED    = 0;
  const ENABLED     = 1;
  const NOT_SELECTED     = -1;
  
  const ADDRESS_SHIPPING    = 0;
  const ADDRESS_BILLING     = 1;
}
