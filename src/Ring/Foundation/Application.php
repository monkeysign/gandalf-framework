<?php

namespace Ring\Foundation;


class Application {

	/**
	 * @var \Interop\Container\ContainerInterface
	 */
	public $container;

	/**
	 * @var \Illuminate\Routing\Router
	 */
	public $router;

	static function instance( $config = [], $services = [] ) {
		static $instance = null;
		if ( null === $instance ) {
			$instance = new static( $config, $services );
		}

		return $instance;
	}

	/**
	 * App constructor.
	 *
	 * @param $config
	 * @param array $services
	 */
	protected function __construct( $config, $services = [] ) {
		$this->buildContainer( $services, $config );
		$this->container->set( 'config', $config );
	}

	/**
	 * Private clone method to prevent cloning of the instance of the
	 * *Singleton* instance.
	 *
	 * @return void
	 */
	private function __clone() {
	}

	/**
	 * Private unserialize method to prevent unserializing of the *Singleton*
	 * instance.
	 *
	 * @return void
	 */
	private function __wakeup() {
	}

	/**
	 * @param $services
	 * @param $config
	 */
	public function buildContainer( $services, $config ) {

		// add annotation and definition of service
		$builder = new \DI\ContainerBuilder();
		$builder->useAnnotations( false );
		$builder->addDefinitions( $services );
		// set the container
		$this->container = $builder->build();
	}

	/**
	 * @return \Interop\Container\ContainerInterface
	 */
	public function getContainer() {
		return $this->container;
	}

	/**
	 * Dispatch the request through the router
	 */
	public function run() {
		try {
			$response = $this->container->get('router')->dispatch($this->container->get('request'));
		} catch ( \Symfony\Component\HttpKernel\Exception\NotFoundHttpException  $exception ) {
			http_response_code( $exception->getStatusCode() );
			if ( $this->container->has( 'http404' ) ) {
				$this->container->get( 'http404' );
			} else {
				echo '<h1>Error 404, Page not found.</h1>';
			}
			die();
		}
		$response->send();
	}
}