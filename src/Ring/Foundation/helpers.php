<?php
/**
 * App instance
 * @return  \Ring\Foundation\Application
 */
function app() {
	return Ring\Foundation\Application::instance();
}

/**
 * Container
 * @return \Interop\Container\ContainerInterface
 */
function container() {
	return app()->container;
}

/**
 * Router
 * @return \Illuminate\Routing\Router
 * @throws \Psr\Container\ContainerExceptionInterface
 * @throws \Psr\Container\NotFoundExceptionInterface
 */
function router() {
	return container()->get( 'router' );
}

/**
 * Request
 * @return \Illuminate\Http\Request
 * @throws \Psr\Container\ContainerExceptionInterface
 * @throws \Psr\Container\NotFoundExceptionInterface
 */
function request() {
	return container()->get( 'request' );
}

/**
 * Redirector
 * @return \Illuminate\Routing\Redirector
 * @throws \Psr\Container\ContainerExceptionInterface
 * @throws \Psr\Container\NotFoundExceptionInterface
 */
function redirector() {
	return container()->get( 'redirector' );
}

/**
 * Database
 * @return \Illuminate\Database\Capsule\Manager
 * @throws \Psr\Container\ContainerExceptionInterface
 * @throws \Psr\Container\NotFoundExceptionInterface
 */
function db() {
	return container()->get( 'db' );
}

/**
 * @return \Ring\View\View
 * @throws \Psr\Container\ContainerExceptionInterface
 * @throws \Psr\Container\NotFoundExceptionInterface
 */
function view() {
	return container()->get( 'view' );
}

/**
 * @return \Symfony\Component\HttpFoundation\Session\Session
 * @throws \Psr\Container\ContainerExceptionInterface
 * @throws \Psr\Container\NotFoundExceptionInterface
 */
function session() {
	return container()->get( 'session' );
}

/**
 * @return \Ring\Hooks\Hook
 * @throws \Psr\Container\ContainerExceptionInterface
 * @throws \Psr\Container\NotFoundExceptionInterface
 */
function hooks() {
	return container()->get( 'hooks' );
}

/**
 * Abort with custom http code
 *
 * @param $code
 * @param $function
 */
function abort( $code, $function = null ) {
	http_response_code( $code );
	if ( is_callable( $function ) ) {
		$function();
	}
	die();
}

/**
 * @return Illuminate\Routing\UrlGenerator
 * @throws \Psr\Container\ContainerExceptionInterface
 * @throws \Psr\Container\NotFoundExceptionInterface
 */
function url() {
	if ( ! container()->has( 'url' ) ) {
		container()->set( 'url', new Illuminate\Routing\UrlGenerator( router()->getRoutes(), request() ) );
	}

	return container()->get( 'url' );
}

/**
 * Torna l'url della route con nome
 *
 * @param $name
 * @param $parameters
 * @param bool $absolute
 *
 * @throws \Psr\Container\ContainerExceptionInterface
 * @throws \Psr\Container\NotFoundExceptionInterface
 */
function path_for( $name, $parameters = [], $absolute = true ) {
	return url()->route( $name, $parameters, $absolute );
}

/**
 * Return a config value
 *
 * @param null $key
 *
 * @return bool
 * @throws \Psr\Container\ContainerExceptionInterface
 * @throws \Psr\Container\NotFoundExceptionInterface
 */
function config( $key = null ) {
	if ( is_null( $key ) ) {
		return false;
	}

	return container()->get( 'config' )[ $key ];
}