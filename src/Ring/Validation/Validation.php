<?php

/**
 * Class for the Auth to the App
 *
 * @author   Tartaglia Riccardo <tartaglia.riccardo@gmail.com>
 */

namespace Ring\Validation;
use Ring\Exception\ValidationException;

class Validation {

    protected $c;

    function __construct($c) {
        $this->c = $c;
    }

    function String($name, $value, $mandatory = false, $min_len = 0, $max_len = 250) {
        if (($mandatory === true) && (boolval($value) === false)) {
            throw new ValidationException($this->getMessage($name, "error_mandatory"), 0, null, $name);
        } else {
            if (strlen($value) > 0) {
                if (strlen($value) <= $min_len) {
                    throw new ValidationException($this->getMessage($name, "error_string_min_length"), 0, null, $name);
                } elseif (strlen($value) > $max_len) {
                    throw new ValidationException($this->getMessage($name, "error_string_max_length"), 0, null, $name);
                }
            }
        }
    }
    
    function Numeric($name, $value, $mandatory = false) {
    	if (($mandatory === true) && (boolval($value) === false)) {
    		throw new ValidationException($this->getMessage($name, "error_mandatory"), 0, null, $name);
    	} else {    	
    		if ((strlen($value) > 0) && (!is_numeric($value))) {
    			throw new ValidationException($this->getMessage($name, "error_numeric"), 0, null, $name);
    		}    		
    	}
    }
    
    function NumericLowerBound($name, $value, $min_value = 0) {    	
    	if ($value < $min_value){
    		throw new ValidationException($this->getMessage($name, "error_min_value",$min_value), 0, null, $name);
    	}
    }
    
    function NumericUpperBound($name, $value, $max_value) {
    	if ($value > $max_value) {
    		throw new ValidationException($this->getMessage($name, "error_max_value",$max_value), 0, null, $name);
    	}
    }

    function Email($name, $value, $mandatory = false, $min_len = 0, $max_len = 250) {
        if (($mandatory === true) && (boolval($value) === false)) {
            throw new ValidationException($this->getMessage($name, "error_mandatory"), 0, null, $name);
        } else {
            if (filter_var($value, FILTER_VALIDATE_EMAIL)) {
                if (strlen($value) <= $min_len) {
                    throw new ValidationException($this->getMessage($name, "error_string_min_length"), 0, null, $name);
                } elseif (strlen($value) > $max_len) {
                    throw new ValidationException($this->getMessage($name, "error_string_max_length"), 0, null, $name);
                }
            } else {
                throw new ValidationException($this->getMessage($name, "error_email_not_valid"), 0, null, $name);
            }
        }
    }
    
    function Band($name, $item) {
    	$band1 = array();
    	$band2 = array();
    	for ($i = 1; $i <= $item['indice']; $i++) {
    		if ((boolval($item['band1_' . $i]) === false) || !(is_numeric($item['band1_' . $i])))
    			throw new ValidationException($this->getMessage($name, "error_band"), 0, null, $name);
    			if ((boolval($item['band2_' . $i]) === false) || !(is_numeric($item['band2_' . $i])))
    				throw new ValidationException($this->getMessage($name, "error_band"), 0, null, $name);
    				$band1[] = $item['band1_' . $i];
    				$band2[] = $item['band2_' . $i];
    	}
    
    	//i due array devono essere crescenti
    	for ($i = 0; $i < $item['indice'] - 1; $i++) {
    		for ($j = $i + 1; $j < $item['indice']; $j++) {
    			if ($band1[$i] > $band1[$j])
    				throw new ValidationException($this->getMessage($name, "error_band"), 0, null, $name);
    				if ($band2[$i] > $band2[$j])
    					throw new ValidationException($this->getMessage($name, "error_band"), 0, null, $name);
    					if ($band2[$i] > $band1[$j])
    						throw new ValidationException($this->getMessage($name, "error_band"), 0, null, $name);
    						if ($band1[$i] > $band2[$j])
    							throw new ValidationException($this->getMessage($name, "error_band"), 0, null, $name);
    		}
    	}
    
    	//controllo che i due termini degli estremi non siano gli stessi
    	for ($i = 0; $i < $item['indice']; $i++) {
    		if ($band1[$i] >= $band2[$i])
    			throw new ValidationException($this->getMessage($name, "error_band"), 0, null, $name);
    	}
    
    
    }
    
    function getMessage($name, $error, $value = "") {
        return $name . ": " . $error . " " . $value;
    }
    
}
