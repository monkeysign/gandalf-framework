<?php

/**
 * Class for the Auth to the App
 *
 */

namespace Ring\Exception;

/**
 * Define a custom exception class
 */
class ValidationException extends \Exception
{
    public function __construct($message, $code = 0, \Exception $previous = null) {
        parent::__construct($message, $code, $previous);
    }

    // custom string representation of object
    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }

    public function getErrorCode() {
        return $this->message;
    }
    
    public function logMexError (){
        $lastTrace = explode('#1',$this->getTraceAsString())[0];
        return "{$this->message}\n{$lastTrace}";
    }
}
