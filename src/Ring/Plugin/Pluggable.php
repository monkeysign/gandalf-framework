<?php
namespace Ring\Plugin;
use Ring\Foundation\Application as Application;


class Pluggable {

	/**
	 * Register action
	 */
	protected static function register(){
	}

	protected static function addPartial($path){
		view()->bladeCompiler->compile($path);
		require view()->bladeCompiler->getCompiledPath($path);
	}
}