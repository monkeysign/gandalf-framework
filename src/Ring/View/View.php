<?php
/**
 * Created by PhpStorm.
 * User: Riccardo
 * Date: 22/01/2018
 * Time: 10:58
 */

namespace Ring\View;

use Illuminate\Container\Container;
use Illuminate\Events\Dispatcher;
use Illuminate\Filesystem\Filesystem;
use Illuminate\View\Compilers\BladeCompiler;
use Illuminate\View\Engines\CompilerEngine;
use Illuminate\View\Engines\EngineResolver;
use Illuminate\View\Engines\PhpEngine;
use Illuminate\View\Factory;
use Illuminate\View\FileViewFinder;
use Gandalf\Ring\Foundation;

class View {
	private $paths;
	private $compilated;
	private $filesystem;
	private $eventDispatcher;

	private $viewResolver;
	public  $bladeCompiler;

	public $viewFinder;
	private $viewFactory;

	/**
	 * View constructor.
	 *
	 * @param $paths
	 * @param $compilated
	 */
	public function __construct($paths, $compilated) {
		$this->paths = $paths;
		$this->compilated = $compilated;

		$this->filesystem = new Filesystem;
		$this->eventDispatcher = new Dispatcher(new Container);
		// Create View Factory capable of rendering PHP and Blade templates
		$this->viewResolver = new EngineResolver;
		$this->bladeCompiler = new BladeCompiler( $this->filesystem, $this->compilated );

		$this->viewResolver->register('blade', function () {
			return new CompilerEngine($this->bladeCompiler, $this->filesystem);
		});
		$this->viewResolver->register('php', function () {
			return new PhpEngine;
		});

		$this->viewFinder = new FileViewFinder($this->filesystem, $this->paths);
		$this->viewFactory = new Factory($this->viewResolver, $this->viewFinder, $this->eventDispatcher);
	}

	/**
	 * Render a new View
	 * @param $data
	 */
	public function render($name, $data){
		echo $this->viewFactory->make($name, $data)->render();
	}

	/**
	 * Build a string
	 * @param $string
	 * @param $data
	 *
	 * @return string
	 * @throws \Exception
	 */
	public function build($string, $data)
	{
		$php = $this->bladeCompiler->compileString($string);

		$obLevel = ob_get_level();
		ob_start();
		extract($data, EXTR_SKIP);

		try {
			eval('?' . '>' . $php);
		} catch (Exception $e) {
			while (ob_get_level() > $obLevel) ob_end_clean();
			throw $e;
		} catch (Throwable $e) {
			while (ob_get_level() > $obLevel) ob_end_clean();
			throw new FatalThrowableError($e);
		}

		return ob_get_clean();
	}

}