<?php

namespace Ring\Load;

use Illuminate\Events\Dispatcher;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Routing\Router;
use Illuminate\Routing\UrlGenerator;
use Ring\Foundation\Application as Application;

class Loader {

	private $app;
	private $routes;

	public function __construct( Application $app ) {
		$this->app = $app;
	}

	/**
	 * Load Modules
	 *
	 * @param string $name
	 */
	public function loadModule( $name = '/*/' ) {
		$modules       = [];
		$modulesFolder = array_filter( glob( config( 'modules_folder' ) . $name ), "is_dir" );
		foreach ( $modulesFolder as $folder ) {
			$conf                          = require $folder . 'config/config.php';
			$modules[ $conf['name'] ]      = $conf;
			$this->routes[ $conf['name'] ] = $conf['routes'];
			$conf['class']::register();
		}
		$this->app->container->set( 'modules', $modules );
	}

	/**
	 * Load Plugins
	 *
	 * @param string $name
	 */
	public function loadPlugin( $name = '/*/' ) {
		$plugins       = [];
		$modulesFolder = array_filter( glob( config( 'plugins_folder' ) . $name ), "is_dir" );
		foreach ( $modulesFolder as $folder ) {
			$conf = require $folder . 'config/config.php';
			//load sub plugin
			if(isset($conf['hasSub']) && $conf['hasSub'] == true){
				$subFolder = $conf['sub'];
				foreach ( $subFolder as $sub ) {
					$confSub = require $folder . '/' . $sub . '/config/config.php';
					$plugins[ $confSub['name'] ]      = $confSub;
					$this->routes[ $confSub['name'] ] = $confSub['routes'];
					$confSub['class']::register();
				}
			}
			$plugins[ $conf['name'] ]      = $conf;
			$this->routes[ $conf['name'] ] = $conf['routes'];
			$conf['class']::register();
		}
		$this->app->container->set( 'plugins', $plugins );
	}

	public function loadRoute() {
		// Create a request from server variables, and bind it to the container; optional
		$request = Request::capture();
		container()->set( 'request', $request );
		// Using Illuminate/Events/Dispatcher here (not required); any implementation of
		// Illuminate/Contracts/Event/Dispatcher is acceptable
		$events = new Dispatcher();
		// Create the router instance
		$router = new Router( $events );
		container()->set( 'router', $router );
		// load routes
		foreach ( $this->routes as $route ) {
			require $route;
		}
		// Create the redirect instance
		$redirect = new Redirector(new UrlGenerator( $router->getRoutes(), $request ) );
		container()->set( 'redirector', $redirect );
	}
}